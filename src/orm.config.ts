import { join } from 'path';
import config from './config';

module.exports = {
  type: 'postgres',
  url: config.ENV ? config.DB_URL_TEST : config.DB_URL,
  autoLoadEntities: true,
  synchronize: config.ENV,
  logger: config.ENV ? 'debug' : 'file',
  // // encrypt: false,
  // ssl: {
  //   // rejectUnauthorized: false,
  //   ca: config.DB_SSL_CERT,
  // },
  // // options: { encrypt: false },
  entities: [join(__dirname, './modules/**/entities/*.entity{.ts,.js}')],
  migrations: [join(__dirname, './migrations/*{.ts,.js}')],
};
