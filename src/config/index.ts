import * as dotenv from 'dotenv';
dotenv.config();

export default {
  PORT: +process.env.PORT,
  JWT_KEY: process.env.JWT_KEY,
  JWT_RF_EXPIRES: process.env.JWT_RF_EXPIRES,
  JWT_AC_EXPIRES: process.env.JWT_AC_EXPIRES,
  DB_URL: process.env.DB_URL,
  DB_URL_TEST: process.env.DB_URL_TEST,
  DB_URL_FOR_UNIT_TESTING: process.env.DB_URL_FOR_UNIT_TESTING,
  ENV: process.env.ENV === 'DEV',
  LIMIT_PAGE: process.env.LIMIT_PAGE,
  FILE_SIZE_IMG: +process.env.FILE_SIZE_IMG,
  FILE_SIZE_IMG_VIDEO: +process.env.FILE_SIZE_IMG_VIDEO,
  FILE_SIZE_IMG_PDF: +process.env.FILE_SIZE_IMG_PDF,
  FILE_SIZE_IMG_VIDEO_PDF_CVS: +process.env.FILE_SIZE_IMG_VIDEO_PDF_CVS,
  FILE_SIZE_ALL: +process.env.FILE_SIZE_ALL,
  COOKIE_EXPIRES: new Date(Date.now() + 3600 * 1000 * 24 * 100),
};
