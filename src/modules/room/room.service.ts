import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CONNECTION } from 'src/basic/constanta/connection.const';
import { BasicService } from 'src/basic/service/service.generic';
import { Connection, Repository } from 'typeorm';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';
import { Room } from './entities/room.entity';

@Injectable()
export class RoomService extends BasicService<CreateRoomDto, UpdateRoomDto> {
  constructor(@Inject(CONNECTION) connection: Connection) {
    super('Room', Room, connection);
  }
}
