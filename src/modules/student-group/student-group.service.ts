import { Inject, Injectable } from '@nestjs/common';
import { CONNECTION } from 'src/basic/constanta/connection.const';
import { BasicService } from 'src/basic/service/service.generic';
import { Connection, Repository } from 'typeorm';
import { CreateStudentGroupDto } from './dto/create-student-group.dto';
import { UpdateStudentGroupDto } from './dto/update-student-group.dto';
import { StudentGroup } from './entities/student-group.entity';

@Injectable()
export class StudentGroupService extends BasicService<
  CreateStudentGroupDto,
  UpdateStudentGroupDto
> {
  constructor(@Inject(CONNECTION) connection: Connection) {
    super('Student Group', StudentGroup, connection);
  }
}
