import { UserGenericEntity } from 'src/basic/entity/user.generic.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Group } from '../../group/entities/group.entity';
import { User } from '../../user/entities/user.entity';

@Entity({ name: 'student_groups' })
export class StudentGroup extends UserGenericEntity {
  @ManyToOne(() => User, (user) => user.groups, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @ManyToOne(() => Group, (group) => group.students, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;
}
