import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, IsNumber, Min } from "class-validator";

export class CreateStudentGroupDto {
  @ApiProperty({
    type: Number,
    description: 'user id',
  })
  @IsNotEmpty()
  @IsNumber()
  user: number;

  @ApiProperty({
    type: Number,
    description: 'group id',
  })
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  group: number;
}
