import { UserGenericEntity } from 'src/basic/entity/user.generic.entity';
import { User } from 'src/modules/user/entities/user.entity';
import { Column, Entity, OneToMany } from 'typeorm';

@Entity({ name: 'roles' })
export class Role extends UserGenericEntity {
  @Column({
    name: 'name',
    type: 'varchar',
    nullable: false,
    length: 255,
    unique: true,
  })
  name: string;

  @OneToMany(() => User, (user) => user.role)
  users: User[];
}
