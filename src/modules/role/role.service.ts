import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CONNECTION } from 'src/basic/constanta/connection.const';
import { BasicService } from 'src/basic/service/service.generic';
import { Connection, Repository } from 'typeorm';
import { Room } from '../room/entities/room.entity';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';

@Injectable()
export class RoleService extends BasicService<CreateRoleDto, UpdateRoleDto> {
  constructor(@Inject(CONNECTION) connection: Connection) {
    super('Role', Role, connection);
  }
}
