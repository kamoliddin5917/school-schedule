import { UserGenericEntity } from 'src/basic/entity/user.generic.entity';
import { Schedule } from 'src/modules/schedule/entities/schedule.entity';
import { Column, Entity, OneToMany } from 'typeorm';

@Entity({ name: 'subjects' })
export class Subject extends UserGenericEntity {
  @Column({
    name: 'name',
    type: 'varchar',
    nullable: false,
    length: 255,
    unique: true,
  })
  name: string;

  @OneToMany(() => Schedule, (schedule) => schedule.subject)
  schedules: Schedule[];
}
