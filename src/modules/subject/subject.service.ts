import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CONNECTION } from 'src/basic/constanta/connection.const';
import { BasicService } from 'src/basic/service/service.generic';
import { Connection, Repository } from 'typeorm';
import { CreateSubjectDto } from './dto/create-subject.dto';
import { UpdateSubjectDto } from './dto/update-subject.dto';
import { Subject } from './entities/subject.entity';

@Injectable()
export class SubjectService extends BasicService<
  CreateSubjectDto,
  UpdateSubjectDto
> {
  constructor(@Inject(CONNECTION) connection: Connection) {
    super('subject', Subject, connection);
  }
}
