import { UserGenericEntity } from 'src/basic/entity/user.generic.entity';
import { Schedule } from 'src/modules/schedule/entities/schedule.entity';
import { StudentGroup } from 'src/modules/student-group/entities/student-group.entity';
import { Column, Entity, OneToMany } from 'typeorm';

@Entity({ name: 'groups' })
export class Group extends UserGenericEntity {
  @Column({
    name: 'name',
    type: 'varchar',
    nullable: false,
    length: 255,
    unique: true,
  })
  name: string;

  @OneToMany(() => StudentGroup, (studentGroup) => studentGroup.group)
  students: StudentGroup[];

  @OneToMany(() => Schedule, (schedule) => schedule.group)
  schedules: Schedule[];
}
