import { UserGenericEntity } from 'src/basic/entity/user.generic.entity';
import { LessonTime } from 'src/basic/enum/lesson-time.enum';
import { WeekDay } from 'src/basic/enum/week-day.enum';
import { Group } from 'src/modules/group/entities/group.entity';
import { Room } from 'src/modules/room/entities/room.entity';
import { Subject } from 'src/modules/subject/entities/subject.entity';
import { User } from 'src/modules/user/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity({ name: 'schedules' })
export class Schedule extends UserGenericEntity {
  @Column({ name: 'week_day', type: 'enum', enum: WeekDay, nullable: false })
  weekDay: WeekDay;

  @Column({
    name: 'lesson_time',
    type: 'enum',
    enum: LessonTime,
    nullable: false,
  })
  lessonTime: LessonTime;

  @ManyToOne(() => Room, (room) => room.schedules, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'room_id' })
  room: Room;

  @ManyToOne(() => Subject, (subject) => subject.schedules, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'subject_id' })
  subject: Subject;

  @ManyToOne(() => Group, (group) => group.schedules, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @ManyToOne(() => User, (user) => user.teacherSchedules, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'teacher_id' })
  teacher: User;

  @ManyToOne(() => User, (user) => user.studentSchedules, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'student_id' })
  student: User;
}
