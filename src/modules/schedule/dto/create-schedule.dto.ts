import { ApiProperty } from '@nestjs/swagger';
import {
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  Min,
} from 'class-validator';
import { LessonTime } from 'src/basic/enum/lesson-time.enum';
import { WeekDay } from 'src/basic/enum/week-day.enum';

export class CreateScheduleDto {
  @ApiProperty({
    enum: WeekDay,
    description: 'weekDay: Monday: 1 , Sunday: 7',
    example: '1',
  })
  @IsNotEmpty()
  @IsNumberString()
  weekDay: WeekDay;

  @ApiProperty({
    enum: LessonTime,
    description: 'lessonTime: FIRST: 1 , SECOND: 2',
    example: '1',
  })
  @IsNotEmpty()
  @IsNumberString()
  lessonTime: LessonTime;

  @ApiProperty({
    type: String,
    description: 'student id',
  })
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  student: number;

  @ApiProperty({
    type: String,
    description: 'teacher id',
  })
  @IsOptional()
  @IsInt()
  @Min(1)
  teacher?: number;

  @ApiProperty({
    type: String,
    description: 'room id',
  })
  @IsOptional()
  @IsInt()
  @Min(1)
  room?: number;

  @ApiProperty({
    type: String,
    description: 'subject id',
  })
  @IsOptional()
  @IsInt()
  @Min(1)
  subject?: number;

  @ApiProperty({
    type: String,
    description: 'group id',
  })
  @IsOptional()
  @IsInt()
  @Min(1)
  group?: number;
}
