import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CONNECTION } from 'src/basic/constanta/connection.const';
import { BasicService } from 'src/basic/service/service.generic';
import { Connection, Repository } from 'typeorm';
import { CreateScheduleDto } from './dto/create-schedule.dto';
import { UpdateScheduleDto } from './dto/update-schedule.dto';
import { Schedule } from './entities/schedule.entity';

@Injectable()
export class ScheduleService extends BasicService<
  CreateScheduleDto,
  UpdateScheduleDto
> {
  constructor(@Inject(CONNECTION) connection: Connection) {
    super('Schedule', Schedule, connection);
  }
}
