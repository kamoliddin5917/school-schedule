import {
  Inject,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { CONNECTION } from 'src/basic/constanta/connection.const';
import { BasicService } from 'src/basic/service/service.generic';
import { Connection, DataSource } from 'typeorm';
import { RoleService } from '../role/role.service';
import { ScheduleService } from '../schedule/schedule.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { ICreateUser } from './interfaces/create-user.interface';

@Injectable()
export class UserService extends BasicService<ICreateUser, UpdateUserDto> {
  private readonly logger = new Logger(UserService.name);

  constructor(@Inject(CONNECTION) connection: Connection) {
    super('user', User, connection);
  }

  @Inject() scheduleService: ScheduleService;
  @Inject() roleService: RoleService;

  async createStudent(createUserDto: CreateUserDto) {
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    this.logger.log('query runner connected');

    await queryRunner.startTransaction();

    try {
      const scheduleServiceCon = this.scheduleService.setConnection(
        this.connection,
      );

      const findStudentRole = await this.roleService.findOneBy({
        where: { name: 'Student' },
      });

      if (!findStudentRole.data) {
        throw new InternalServerErrorException('Student role is not!');
      }

      const newStudent = await this.create({
        fullName: createUserDto.fullName,
        role: findStudentRole.data.id,
      });

      if (!newStudent.data) {
        throw new InternalServerErrorException(
          `This ${createUserDto.fullName} user is not created!`,
        );
      }

      const scheduleValues = [];

      for (let k = 1; k <= 7; k++) {
        for (let d = 1; d <= 12; d++) {
          scheduleValues.push({
            student: newStudent.data.id,
            weekDay: k,
            lessonTime: d,
          });
        }
      }

      await scheduleServiceCon.upsert(scheduleValues);

      return newStudent;
    } catch (error) {
      this.logger.error(error.message);

      await queryRunner.rollbackTransaction();

      throw new InternalServerErrorException(error.message);
    } finally {
      await queryRunner.release();
    }
  }
}
