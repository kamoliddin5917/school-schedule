import { UserGenericEntity } from 'src/basic/entity/user.generic.entity';
import { Role } from 'src/modules/role/entities/role.entity';
import { Schedule } from 'src/modules/schedule/entities/schedule.entity';
import { StudentGroup } from 'src/modules/student-group/entities/student-group.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

@Entity({ name: 'users' })
export class User extends UserGenericEntity {
  @Column({
    name: 'full_name',
    type: 'varchar',
    nullable: false,
    length: 255,
    unique: true,
  })
  fullName: string;

  @ManyToOne(() => Role, (role) => role.users, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'role_id' })
  role: Role;

  @OneToMany(() => StudentGroup, (studentGroup) => studentGroup.user)
  groups: StudentGroup[];

  @OneToMany(() => Schedule, (schedule) => schedule.teacher)
  teacherSchedules: Schedule[];

  @OneToMany(() => Schedule, (schedule) => schedule.student)
  studentSchedules: Schedule[];
}
