import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    type: String,
    description: 'full name',
  })
  @IsNotEmpty()
  @IsString()
  fullName: string;
}
