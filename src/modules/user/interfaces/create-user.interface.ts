export interface ICreateUser {
  fullName: string;
  role: number;
}
