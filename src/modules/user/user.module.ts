import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { RoleModule } from '../role/role.module';
import { Schedule } from '../schedule/entities/schedule.entity';
import { ScheduleModule } from '../schedule/schedule.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Schedule]),
    RoleModule,
    ScheduleModule,
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
