import { Column } from 'typeorm';
import { DateGenericEntity } from './date.generic.entity';

export class UserGenericEntity extends DateGenericEntity {
  @Column({ name: 'created_by', type: 'int', nullable: true })
  createdBy: number;

  @Column({ name: 'updated_by', type: 'int', nullable: true })
  updatedBy: number;

  @Column({ name: 'deleted_by', type: 'int', nullable: true })
  deletedBy: number;
}
