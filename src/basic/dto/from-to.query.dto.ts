import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDate, IsNotEmpty } from 'class-validator';
import { BranchQueryDto } from './branch.query.dto';

export class FromToDto extends BranchQueryDto {
  @ApiProperty({
    type: Date,
    description: 'from date',
  })
  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  from: Date;

  @ApiProperty({
    type: Date,
    description: 'to date',
  })
  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  to: Date;
}
