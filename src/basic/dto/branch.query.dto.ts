import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNumberString, IsOptional } from 'class-validator';

class BranchQuery {
  @ApiProperty({
    type: String,
    description: 'branchId',
  })
  @IsOptional()
  @IsNumberString()
  branch?: string;
}

export class BranchQueryDto extends PartialType(BranchQuery) {}
