import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsNumber } from 'class-validator';

export class UpdateManyToManyTableDto {
  @ApiProperty({
    type: [Number],
    description: `added id`,
  })
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  added: number[];

  @ApiProperty({
    type: [Number],
    description: `added id`,
  })
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  deleted: number[];
}
