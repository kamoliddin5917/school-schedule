/**
 * Current user interface
 * @interface ICurrentUser - Interface
 * * id - number
 * * schema - string
 * * branch - number
 */
export class ICurrentUser {
  id: number;
  schema?: string;
  branchId?: number;
}
