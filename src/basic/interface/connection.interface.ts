import { DataSource } from 'typeorm';

export type Connection = DataSource;
