/**
 * BasicResponse is to generate messages for any statuses.
 * * message: string[ ];
 * * statusCode: number;
 * * data: any;
 * * error: string;
 */
export class BasicResponse {
  message: string[];
  statusCode: number;
  data: any;
  error?: string;

  constructor(
    message: string[],
    statusCode: number,
    data?: any,
    error?: string,
  ) {
    this.message = message;
    this.statusCode = statusCode;
    this.data = data;
    this.error = error;
  }
}
