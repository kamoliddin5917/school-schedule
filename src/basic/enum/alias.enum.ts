export enum ROLES {
  ADMIN = 'ADMIN',
  STUDENT = 'STUDENT',
  TEACHER = 'TEACHER',
  PARENT = 'PARENT',
}

export enum RATE_STATUSES {
  KELDI = 'KELDI',
  KEMADI = 'KEMADI',
}

export enum RATE_TYPES {
  ABCD = 'ABCD',
  SELECTED = 'SELECTED',
  INPUT = 'INPUT',
}

export enum FILE_TYPES {
  AVATAR = 'Avatar',
  SPRAFKA = 'SPRAFKA',
}
