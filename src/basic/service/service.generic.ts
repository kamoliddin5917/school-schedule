import { Connection, Repository } from 'typeorm';
import { ICurrentUser } from '../interface/current-user.interface';
import { IFind, IFindBy, IFindOne } from '../interface/find.interface';
import { BasicResponse } from '../interface/res.basic.interface';

/**
 * Base service
 * Methods:
 * 1. findAll
 * 2. create
 * 3. update
 * 4. remove
 * 5. sofDelete
 * 6. findOne
 * 7. findOneBy
 */
export class BasicService<CreateDto, UpdateDto> {
  readonly repository: Repository<any>;

  /**
   * @param param - what is worked with
   * @param entity - entity
   * @param connection - connection
   */
  constructor(
    private param: string,
    private entity: any,
    protected connection: Connection,
  ) {
    this.param = param;
    this.repository = connection.getRepository(entity);
  }

  /**
   * conncetion setter function
   *
   * @param _connection connection
   */
  setConnection(_connection: Connection) {
    this.connection = _connection;

    return this;
  }

  /**
   *
   * @param options - typeOrm find method options
   * @param options.select - column name
   * @param options.relations - relation columns
   * @param options.where - where options
   * @returns
   ** message - [text]
   ** [data]
   ** statusCode - 204 or 200
   */
  async findAll(options?: IFind): Promise<BasicResponse> {
    const findAllData = await this.repository.find({
      select: options?.select || {},
      relations: options?.relations || [],
      where: options?.where || {},
      order: options?.order || {},
    });

    if (!findAllData.length)
      return new BasicResponse([this.param + ' not found'], 204, findAllData);

    return new BasicResponse([this.param + ' all data'], 200, findAllData);
  }

  /**
   *
   * @param dto - create element object
   * @param currentUser - current user
   * 1. id
   * 2. schema
   * 3. branchId
   * @returns
   ** message - [text]
   ** data
   ** statusCode - 204 or 200
   */
  async create(
    dto: CreateDto,
    currentUser?: ICurrentUser,
  ): Promise<BasicResponse> {
    let createdData = this.repository.create({
      ...dto,
      createdBy: currentUser?.id,
    });

    createdData = await this.repository.save(createdData);

    return new BasicResponse(
      [this.param + ' created successfuly'],
      201,
      createdData,
    );
  }

  /**
   * Updates the data
   *
   * @param id - Id to find the data
   * @param dto - update the data
   * @param currentUser - who updates the data
   * 1. id
   * 2. schema
   * 3. branchId
   * @returns
   ** message - [text]
   ** data
   ** statusCode - 204 or 200
   */
  async update(
    id: number,
    dto: UpdateDto,
    currentUser?: ICurrentUser,
  ): Promise<BasicResponse> {
    let updatedData = await this.findOne(id);

    if (!updatedData || !updatedData.data)
      return new BasicResponse(
        [this.param + ' not found'],
        204,
        updatedData.data,
      );

    let key: string;

    for (key in dto) {
      updatedData.data[key] = dto[key];
    }

    updatedData.data.updatedAt = new Date();

    if (currentUser?.id) {
      updatedData.data.updatedBy = currentUser.id;
    }

    updatedData = await this.repository.save(updatedData.data);

    return new BasicResponse(
      [this.param + ' updated succesfuly'],
      200,
      updatedData,
    );
  }

  /**
   *
   * @param id - Id to find the data
   * @returns
   ** message - [text]
   ** statusCode - 200
   ** data - object
   */
  async remove(id: number): Promise<BasicResponse> {
    const removed = await this.repository.delete(id);

    return new BasicResponse(
      [this.param + ' deleted successfuly'],
      200,
      removed,
    );
  }

  /**
   *
   * @param options - column name = value
   * @returns
   ** message - [text]
   ** statusCode - 200
   ** data - object
   */
  async removeBy(options: object): Promise<BasicResponse> {
    const removed = await this.repository.delete(options);

    return new BasicResponse(
      [this.param + ' deleted successfuly'],
      200,
      removed,
    );
  }

  /**
   *
   * @param id - Id to find the data
   * @param currentUser - who updates the data
   * @returns
   ** message - [text]
   ** data
   ** statusCode - 204 or 200
   */
  async sofDelete(
    id: number,
    currentUser?: ICurrentUser,
  ): Promise<BasicResponse> {
    let sofDelete = await this.findOne(id);

    if (!sofDelete || !sofDelete.data)
      return new BasicResponse(
        [this.param + ' not found'],
        204,
        sofDelete.data,
      );

    sofDelete.data.deletedAt = new Date();

    sofDelete.data.status = false;

    if (currentUser?.id) {
      sofDelete.data.deletedBy = currentUser.id;
    }

    sofDelete = await this.repository.save(sofDelete.data);

    return new BasicResponse(
      [this.param + ' deleted successfuly'],
      200,
      sofDelete,
    );
  }

  /**
   *
   * @param id - Id to find the data
   * @param options - optional
   * @param options.select - optional
   * @param options.relations - optional
   * @returns
   ** message - [text]
   ** data
   ** statusCode - 204 or 200
   */
  async findOne(id: number, options?: IFindOne): Promise<BasicResponse> {
    const findOne = await this.repository.findOne({
      select: options?.select || [],
      relations: options?.relations || [],
      where: { id },
    });

    if (!findOne)
      return new BasicResponse([this.param + ' not found'], 204, findOne);

    return new BasicResponse([this.param + ' data'], 200, findOne);
  }

  /**
   *
   * @param options - typeOrm find method options
   * @param options.select - column name
   * @param options.relations - relation columns
   * @param options.where - where options
   * @returns
   ** message - [text]
   ** data
   ** statusCode - 204 or 200
   */

  async findOneBy(options: IFindBy): Promise<BasicResponse> {
    const findOneBy = await this.repository.findOne({
      select: options.select || [],
      relations: options.relations || [],
      where: options.where,
    });

    if (!findOneBy)
      return new BasicResponse([this.param + ' not found'], 204, findOneBy);

    return new BasicResponse([this.param + ' data'], 200, findOneBy);
  }

  /**
   *
   * @param values - create element object in array
   * @param currentUser - who updates the data
   * @param conflictColumns - conflict column name
   * @returns
   ** message - [text]
   ** data
   ** statusCode - 201
   */
  async upsert(
    values: CreateDto[],
    currentUser?: ICurrentUser,
    conflictColumns?: string[],
  ) {
    const createdByAddValues = values.map((iteam) => ({
      ...iteam,
      createdBy: currentUser?.id || null,
    }));

    const createdData = await this.repository.upsert(
      createdByAddValues,
      conflictColumns || ['id'],
    );

    return new BasicResponse(
      [this.param + ' created succesfuly'],
      201,
      createdData,
    );
  }
}
