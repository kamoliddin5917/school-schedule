import 'reflect-metadata';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';
import { Logger, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import config from './config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({
    origin: '*',
    credentials: true,
    exposedHeaders: 'x-authorization',
  });
  app.getHttpAdapter().getInstance().disable('x-powered-by');
  app.getHttpAdapter().getInstance().disable('etag');

  const options = new DocumentBuilder()
    .setTitle('School System 2')
    .setDescription('/api-swagger')
    .setVersion('0.0.1')
    .addTag('auth')
    .addApiKey(
      { type: 'apiKey', name: 'x-tenant-id', in: 'header' },
      'x-tenant-id',
    )
    .build();

  app.setGlobalPrefix('api/v1');

  app.useGlobalPipes(
    new ValidationPipe({ whitelist: true, forbidNonWhitelisted: true }),
  );

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-swagger', app, document, {
    swaggerOptions: {
      persistAuthorization: true,
    },
  });

  app.use(bodyParser.json({ limit: '20mb' }));
  app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }));

  await app.listen(config.PORT, () => {
    Logger.log(
      `Server is running on http://localhost:${config.PORT}`,
      'Server',
    );

    Logger.log(
      `Swagger route: http://localhost:${config.PORT}/api-swagger`,
      'Swagger',
    );

    Logger.log(
      `Api JSON format: http://localhost:${config.PORT}/api-swagger-json`,
      'Swagger Json',
    );
  });
}
bootstrap();
