import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './modules/user/user.module';
import { GroupModule } from './modules/group/group.module';
import { RoomModule } from './modules/room/room.module';
import { SubjectModule } from './modules/subject/subject.module';
import { StudentGroupModule } from './modules/student-group/student-group.module';
import { ScheduleModule } from './modules/schedule/schedule.module';
import { RoleModule } from './modules/role/role.module';
import * as ormconfig from './orm.config';
import { TenancyModule } from './middlewares/tenancy.modules';

@Module({
  imports: [
    TypeOrmModule.forRoot(ormconfig),
    TenancyModule,
    UserModule,
    GroupModule,
    RoomModule,
    SubjectModule,
    StudentGroupModule,
    ScheduleModule,
    RoleModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
